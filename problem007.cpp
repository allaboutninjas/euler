#include <iostream>
#include <vector>
using namespace std;

int main(void) {
	int n = 150000, start = 2, i, j, divisor, primeCounter = 0;
	vector<int> listing(n);
	vector<int> primes;

	for (i = 0; i < n; i++) {
		listing[i] = i;
	}

	for (i = 0; i < n; i++) {
		if (1 >= listing[i]) {
			continue;
		}

		divisor = i;
		primes.push_back(divisor);
		primeCounter++;

		for (j = i; j < n; j++) {
			if (listing[j] % divisor == 0) {
				listing[j] = 1;
			}
		}

		if (primeCounter % 100 == 1) {
			cout << primeCounter << ": " << primes[primeCounter-1] << endl;
		}

		if (10001 == primeCounter) {
			break;
		}
	}

	system("PAUSE");
	return 0;
}