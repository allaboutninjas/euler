#include <iostream>
#include <array>
#include <algorithm>
using namespace std;

int main(void) {
	const int n = 10000;
	int sum = 0;
	array<int, n> arr;
	arr.fill(0);
	for (int k = 1; k < n; k++) {
		// There is no need search up to j because i has to be less than j
		// We can save time if we just divide j by 2 via integer division
		// because there will not be any integer divisors larger than j/2
		// that divide the number evenly.
		for (int i = 1, j = k, sum = 0; i <= (j / 2); i++) {
			if (j % i == 0) {
				arr[k] += i;
			}
		}
	}

	for (int a = 1, b; a < n; a++) {
		b = arr[a];
		if ((b < n) && (a == arr[b]) && (a != b)) {
			sum += a + b;
			arr[a] = 0; // Each pair has to be added only once!
		}
	}
	cout << sum << endl;
	system("PAUSE");
	return 0;
}