#include <iostream>
#include <string>
using namespace std;

string multiplyString(string s, int multiplier);

int main(void) {
	string s = "1";
	int result = 0;
	for (int i = 0; i < 1000; i++) {
		s = multiplyString(s, 2);
	}
	for (int i = 0, n = s.length(); i < n; i++) {
		result += s[i] - '0';
	}
	cout << result << endl;
	system("PAUSE");
	return 0;
}

string multiplyString(string s, int multiplier) {
	string result = "";
	char temp;

	temp = 0;
	for (int j = s.length() - 1; j >= 0; j--) {
		temp += (s[j] - '0') * multiplier;
		result.insert(0, 1, (temp % 10) + '0');
		temp = temp / 10;
	}
	if (temp != 0) {
		result.insert(0, 1, temp + '0');
	}
	return result;
}