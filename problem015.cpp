#include <iostream>
#include <vector>
using namespace std;
// Algorithm found at:
// http://www.robertdickau.com/manhattan.html
// Keyword: shortest-path diagrams, Pascal's triangle

const int size = 20;

int main(void) {
	int m = size, n = size;
	vector<vector<unsigned long long int>>
		graph(m, vector<unsigned long long int>(n, 0));
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			if (i == 0) {
				graph[i][j] += 1;
			}
			else {
				graph[i][j] += graph[i - 1][j];
			}
			if (j == 0) {
				graph[i][j] += 1;
			}
			else {
				graph[i][j] += graph[i][j - 1];
			}
		}
	}
	cout << graph[m - 1][n - 1] << endl;
	system("PAUSE");
	return 0;
}