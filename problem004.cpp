#include <iostream>
#include <algorithm>
#include <string>
#include <sstream>
using namespace std;

bool isPalindrome(string sNum);
string intToStr(int iNum);
int strToInt(string sNum);

int main(void) {
	int maxPalindrome = 0;
	for (int i = 999; i > 900; i--) {
		for (int j = 999; j > 900; j--) {
			if (isPalindrome(intToStr(i*j))) {
				maxPalindrome = max(maxPalindrome, i*j);
			}
		}
	}
	cout << maxPalindrome << endl;
	system("PAUSE");
	return 0;
}

bool isPalindrome(string num) {
	if ((num.length() == 0) || (num.length() == 1)) {
		return true;
	}
	string firstCharacter = num.substr(0, 1);
	string lastCharacter = num.substr(num.length() - 1);
	string restOfString = num.substr(1, num.length() - 2);
	if (firstCharacter == lastCharacter) {
		return (true == isPalindrome(restOfString));
	}
	else
		return false;
}

string intToStr(int iNum) {
	stringstream s;
	s << iNum;
	return s.str();
}

int strToInt(string sNum) {
	stringstream s;
	int number;
	s << sNum;
	s >> number;
	return number;
}