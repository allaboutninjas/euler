#include <iostream>
#include <string>
#include <vector>
using namespace std;

void createDictionary(vector<string>& dict);

int main(void) {
	vector<string> dictionary(101);
	createDictionary(dictionary);
	int thousands = 0, hundreds = 0, tens = 0, ones = 0;
	int totalLetterCount = 0;
	string result = "";
	for (int i = 1; i < 1001; i++, result = "") {
		thousands = (i / 1000) % 10;
		hundreds = (i/100) % 10;
		tens = (i/10) % 10;
		ones = i % 10;
		if (thousands != 0) {
			result += dictionary[thousands] + "thousand";
		}
		if (hundreds != 0) {
			result += dictionary[hundreds] + "hundred";
			if (tens + ones > 0) {
				result += "and";
			}
		}
		if (ones == 0) {
			result += dictionary[tens * 10];
		}
		else {
			if (tens < 2) {
				result += dictionary[tens * 10 + ones];
			}
			else {
				result += dictionary[tens * 10] + dictionary[ones];
			}
		}
		//cout << result << endl;
		totalLetterCount += result.length();
	}
	cout << "Total letter count: " << totalLetterCount << endl;
	system("PAUSE");
	return 0;
}

void createDictionary(vector<string>& dict) {
	dict[1] = "one";
	dict[2] = "two";
	dict[3] = "three";
	dict[4] = "four";
	dict[5] = "five";
	dict[6] = "six";
	dict[7] = "seven";
	dict[8] = "eight";
	dict[9] = "nine";
	dict[10] = "ten";
	dict[11] = "eleven";
	dict[12] = "twelve";
	dict[13] = "thirteen";
	dict[14] = "fourteen";
	dict[15] = "fifteen";
	dict[16] = "sixteen";
	dict[17] = "seventeen";
	dict[18] = "eighteen";
	dict[19] = "nineteen";
	dict[20] = "twenty";
	dict[30] = "thirty";
	dict[40] = "forty";
	dict[50] = "fifty";
	dict[60] = "sixty";
	dict[70] = "seventy";
	dict[80] = "eighty";
	dict[90] = "ninety";
	dict[100] = "hundred";
}