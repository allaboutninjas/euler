#include <iostream>
#include <algorithm>
#include <fstream>
using namespace std;

int checkDownward(int** arr, int index_m, int index_n,
	int howMany = 4, int m = 20, int n = 20);
int checkRightward(int** arr, int index_m, int index_n,
	int howMany = 4, int m = 20, int n = 20);
int checkDiagonallyDownRight(int** arr, int index_m, int index_n,
	int howMany = 4, int m = 20, int n = 20);
int checkDiagonallyDownLeft(int** arr, int index_m, int index_n,
	int howMany = 4, int m = 20, int n = 20);

void readFile(const char* fileName, int** arr, int m = 20, int n = 20);

int main(void) {
	int m = 20, n = 20, result = 0;
	int** arr = new int*[m];
	for (int i = 0; i < m; i++) {
		arr[i] = new int[n];
	}

	readFile("grid.txt", arr);

	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			result = max(result, checkDownward(arr, i, j));
			result = max(result, checkRightward(arr, i, j));
			result = max(result, checkDiagonallyDownRight(arr, i, j));
			result = max(result, checkDiagonallyDownLeft(arr, i, j));
		}
	}

	for (int i = 0; i < m; i++) {
		delete[] arr[i];
	}
	delete[] arr;

	cout << "Result: " << result << endl;
	system("PAUSE");
	return 0;
}

int checkDownward(int** arr, int index_m, int index_n,
	int howMany, int m, int n)
{
	int result = 1;
	if ((index_m >= m) || (index_n >= n)) {
		return -1;
	}

	if ((index_m + howMany) >= m) {
		return -1;
	}

	for (; howMany > 0; howMany--, index_m++) {
		result *= arr[index_m][index_n];
	}
	return result;
}

int checkRightward(int** arr, int index_m, int index_n,
	int howMany, int m, int n)
{
	int result = 1;
	if ((index_m >= m) || (index_n >= n)) {
		return -1;
	}

	if ((index_n + howMany) >= n) {
		return -1;
	}

	for (; howMany > 0; howMany--, index_n++) {
		result *= arr[index_m][index_n];
	}
	return result;
}

int checkDiagonallyDownRight(int** arr, int index_m, int index_n,
	int howMany, int m, int n)
{
	int result = 1;
	if ((index_m >= m) || (index_n >= n)) {
		return -1;
	}

	if (((index_m + howMany) >= m) || ((index_n + howMany) >= n)) {
		return -1;
	}

	for (; howMany > 0; howMany--, index_m++, index_n++) {
		result *= arr[index_m][index_n];
	}
	return result;
}

int checkDiagonallyDownLeft(int** arr, int index_m, int index_n,
	int howMany, int m, int n)
{
	int result = 1;
	if ((index_m >= m) || (index_n >= n)) {
		return -1;
	}

	if (((index_m + howMany) >= m) || ((index_n - howMany) < 0)) {
		return -1;
	}

	for (; howMany > 0; howMany--, index_m++, index_n--) {
		result *= arr[index_m][index_n];
	}
	return result;
}

void readFile(const char* fileName, int** arr, int m, int n) {
	ifstream file(fileName, ios_base::in);
	int temp = 0;
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			file >> arr[i][j];
		}
	}
}