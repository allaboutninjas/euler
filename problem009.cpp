#include <iostream>
#include <cmath>
using namespace std;
const double eps = 1e-6;

int main(void) {
	double fractPart, intPart, tempC;
	int a = 0, b = 1, c = 2;
	while(true) {
		tempC = sqrt(a*a + b*b);
		fractPart = modf(tempC, &intPart);
		if (eps > fractPart) {
			c = (int)tempC;
			if ((a*a + b*b == c*c) && (a + b + c == 1000)) {
				break;
			}
		}
		a--;

		if (a <= 0) {
			b++;
			a = b - 1;
		}
	}

	cout << "Final triplet:" << endl;
	cout << "a: " << a
		<< "\tb: " << b
		<< "\tc: " << c << endl;

	cout << "a^2 + b^2 = " << (a*a + b*b) << " == "
		<< (c*c) << " = c^2" << endl;

	cout << "a*b*c product = " << (a*b*c) << endl;

	system("PAUSE");
	return 0;
}