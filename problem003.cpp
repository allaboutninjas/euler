#include <iostream>
using namespace std;

int main(void) {
	long long start = 600851475143;
	long long maxFactor = 2;
	bool numberFound = false;
	while (!numberFound) {
		if (start % maxFactor == 0) {
			start = start / maxFactor;
		}
		else if (start == 1) {
			numberFound = true;
		}
		else {
			maxFactor++;
		}
	}
	cout << maxFactor << endl;
	system("PAUSE");
	return 0;
}