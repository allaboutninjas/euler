#include <iostream>
#include <vector>
#include <numeric>
using namespace std;

const unsigned int HIGHEST_NUMBER_CONSIDERED = 28123;
bool is_abundant_number(unsigned int number);
unsigned int sum_of_divisors(unsigned int number);

int main(int argc, char* argv[])
{
	vector<unsigned int> numbers(HIGHEST_NUMBER_CONSIDERED + 1);

	for (unsigned int i = 0; i <= HIGHEST_NUMBER_CONSIDERED; ++i)
		numbers[i] = i;

	cout << "Abundancy check started." << endl;
	vector<unsigned int> abundant_numbers;
	for (auto& n : numbers) {
		if (n == 0) continue;
		if (is_abundant_number(n)) {
			abundant_numbers.push_back(n);
		}
	}
	cout << "Abundancy check completed!" << endl;

	cout << "Removing numbers that can be "
		 << "represented as sum of two abundant numbers." << endl;
	for (unsigned int i = 0; i < abundant_numbers.size(); ++i) {
		for (unsigned int j = i; j < abundant_numbers.size(); ++j) {
			auto abundant_sum = abundant_numbers[i] + abundant_numbers[j];
			if (abundant_sum < numbers.size())
				numbers[abundant_sum] = 0;
		}
	}
	cout << "Numbers removed." << endl;
	cout << "Result: ";
	cout << accumulate(numbers.begin(), numbers.end(), 0L) << endl;
	
	system("PAUSE");
	return 0;
}

bool is_abundant_number(unsigned int number)
{
	return sum_of_divisors(number) > number;
}

unsigned int sum_of_divisors(unsigned int number)
{
	auto sum = 1u;
	auto upper_bound = number;
	auto lower_bound = 1u;
	auto pivot = (upper_bound + lower_bound) / 2;

	while (lower_bound < pivot)
	{
		++lower_bound;
		if (number % lower_bound == 0)
		{
			upper_bound = number / lower_bound;
			pivot = (upper_bound + lower_bound) / 2;
			if (upper_bound == lower_bound)
				sum += lower_bound; // same divisor can't be added twice!
			else
				sum += upper_bound + lower_bound;
		}
	}

	return sum;
}