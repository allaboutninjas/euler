#include <iostream>
using namespace std;

int main(void) {
	int start, end, maxLength = 0, sequenceLength, maxNum;
	long long n;
	for (start = 1, end = 1000000, n = 0; start < end; start++) {
		n = start;
		sequenceLength = 1;
		while (n > 1) {
			if (n % 2 == 0) {
				n = n / 2;
			}
			else {
				n = 3 * n + 1;
			}
			sequenceLength++;
		}
		if (maxLength < sequenceLength) {
			maxLength = sequenceLength;
			maxNum = start;
		}
	}
	cout << "Max length of a sequence: " << maxLength
		<< ", for: " << maxNum << endl;
	system("PAUSE");
	return 0;
}