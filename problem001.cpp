#include <iostream>
using namespace std;

int main(void) {
	int sum = 0, i = 0, n = 1000;
	for (i = 0, sum = 0; i < n; i++) {
		if ((i % 3 == 0) || (i % 5 == 0)) {
			sum += i;
		}
	}
	cout << sum << endl;
	system("PAUSE");
	return 0;
}