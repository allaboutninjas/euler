#include <iostream>
#include <string>
using namespace std;

string strDays[] = { "Monday", "Tuesday", "Wednesday", "Thursday", \
                     "Friday", "Saturday", "Sunday" };
int numDays[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

int main(void) {
	int year = 1900;
	int day = 1;
	int month = 1;
	int sundayCounter = 0;
	while (year < 2001) {
		while (month < 13) {
			day += numDays[month];
			if ((month == 2) && \
				(((year % 4 == 0) && (year % 100 != 0)) || \
				(year % 400 == 0)))
			{
				day += 1;
			}
			day %= 7;
			month++;
			if ((day == 0) && (year > 1900)) {
				//cout << month << "/" << year << " - YES!" << endl;
				sundayCounter++;
			}
		}
		month = 1;
		year++;
	}
	cout << "Number of first Sundays: " << sundayCounter << endl;
	system("PAUSE");
	return 0;
}