#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

vector<string> extract_names_from_file(void);
vector<int> convert_names_to_values(const vector<string>& names);
vector<long long> sum_all_values(const vector<string>& names, const vector<int>& name_values);

int main(int argc, char* argv[])
{
	vector<string> names = extract_names_from_file();

	sort(names.begin(), names.end());

	vector<int> name_values = convert_names_to_values(names);

	vector<long long> sums = sum_all_values(names, name_values);

	for (auto& sum : sums)
		cout << sum << endl;

	system("PAUSE");
	return 0;
}


vector<string> extract_names_from_file(void)
{
	vector<string> names;
	ifstream input("names.txt", ios_base::in);
	string new_name = "";
	char c;
	while (input.get(c))
	{
		if (c == '\"')
			continue;
		else if (c == ',')
		{
			names.emplace_back(new_name);
			new_name = "";
		}
		else
			new_name += c;
	}
	if (new_name != "")
		names.emplace_back(new_name);

	return names;
}

vector<int> convert_names_to_values(const vector<string>& names)
{
	vector<int> name_values;
	for (unsigned int i = 1; i <= names.size(); ++i)
	{
		unsigned int temp_name_value = 0;
		for (auto& c : names[i - 1])
		{
			unsigned int char_value = c - 'A' + 1;
			temp_name_value += char_value;
		}
		name_values.emplace_back(temp_name_value * i);
	}

	return name_values;
}

vector<long long> sum_all_values(const vector<string>& names, const vector<int>& name_values)
{
	vector<long long> sums;
	long long sum = 0LL;
	for (unsigned int i = 1; i <= names.size(); ++i)
	{
		long long temp_sum = sum + name_values[i - 1];
		if (temp_sum < 0)
		{
			sums.emplace_back(sum);
			sum = name_values[i - 1];
		}
		else
			sum += name_values[i - 1];
	}
	if (sum != 0)
		sums.emplace_back(sum);

	return sums;
}