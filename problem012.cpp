#include <iostream>
using namespace std;
// Suggestion for the algorithm found at:
// http://www.wikihow.com/Determine-the-Number-of-Divisors-of-an-Integer

int main(void) {
	int n = 500;
	int triangleNumber = 0, i = 0, temp, tempDiv, divisor, nDiv;

	while (true) {
		i++;
		triangleNumber += i;
		divisor = 2;
		nDiv = 1;
		temp = triangleNumber;
		while (temp > 1) {
			tempDiv = 0;
			while (temp % divisor == 0) {
				temp = temp / divisor;
				tempDiv++;
				if (temp % divisor != 0) {
					tempDiv++;
					nDiv *= tempDiv;
				}
			}
			divisor++;
		}
		if (nDiv > 500) {
			break;
		}
	}

	cout << "Final result: " << triangleNumber << endl;
	system("PAUSE");
	return 0;
}