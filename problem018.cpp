#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;

void readFile(string filename, vector<int>& v);

int main(void) {
	vector<int> v;
	readFile("data.txt", v);
	vector<int> path(v.size());
	int result = 0;
	path[0] = v[0];
	for (int i = 0, j = 0, l = 1, n = v.size(); i < n; i++) {
		if ((i + l < v.size()) && \
			(path[i] + v[i + l] > path[i + l])) {
			path[i + l] = path[i] + v[i + l];
			result = max(result, path[i + l]);
		}
		if ((i + l + 1 < v.size()) && \
			(path[i] + v[i + l + 1] > path[i + l + 1])) {
			path[i + l + 1] = path[i] + v[i + l + 1];
			result = max(result, path[i + l + 1]);
		}
		cout << path[i] << " ";
		j++;
		if (j == l) {
			cout << endl;
			l++;
			j = 0;
		}
	}
	cout << "Maximum total: " << result << endl;
	system("PAUSE");
	return 0;
}

void readFile(string filename, vector<int>& v) {
	ifstream in(filename, ios_base::in);
	int temp;
	while (in.good()) {
		in >> temp;
		v.push_back(temp);
	}
}