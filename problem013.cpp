#include <iostream>
#include <fstream>
#include <string>
using namespace std;

string addStrings(string s1, string s2);
char addChar(char c1, char c2, char c3='0');

int main(void) {
	string previousResult = "0", fStr = "";
	fstream file("numbers.txt", ios_base::in);
	while (file.good()) {
		file >> fStr;
		previousResult = addStrings(previousResult, fStr);
	}
	cout << previousResult << endl;
	system("PAUSE");
	return 0;
}

string addStrings(string s1, string s2) {
	string result = "";
	if ((s1.length() > s2.length()) || (s1.length() < s2.length())) {
		if (s1.length() < s2.length()) {
			while (s1.length() != s2.length()) {
				s1.insert(0, "0");
			}
		}
		else {
			while (s2.length() != s1.length()) {
				s2.insert(0, "0");
			}
		}
	}
	char temp = '0';
	for (int i = s1.length()-1; i >= 0; i--) {
		temp = addChar(s1[i], s2[i], temp);
		if (temp > '9') {
			temp -= 10;
			result.insert(0, 1, temp);
			temp = '1';
		}
		else {
			result.insert(0, 1, temp);
			temp = '0';
		}
	}
	if (temp > '0') {
		result.insert(0, 1, temp);
	}
	return result;
}

char addChar(char c1, char c2, char c3) {
	return c1 + (c2 - '0') + (c3 - '0');
}