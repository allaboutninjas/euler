#include <iostream>
using namespace std;

int main(void) {
	int sumOfSquares = 0;
	int sum = 0;

	for (int i = 1; i <= 100; i++) {
		sumOfSquares += i*i;
		sum += i;
	}

	cout << (sum*sum) - sumOfSquares << endl;

	system("PAUSE");
	return 0;
}