#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
using namespace std;

string readFile(string fileName);
bool readFive(string* strNum, int* arr);

int main(void) {
	int arr[5] = { 0, 0, 0, 0, 0 }, maxProd = 0, i, temp;
	string bigNumber = readFile("big_number.txt");
	while (readFive(&bigNumber, &arr[0])) {
		for (i = 0, temp = 1; i < 5; i++) {
			temp *= arr[i];
		}
		maxProd = max(maxProd, temp);
		bigNumber = bigNumber.substr(1, bigNumber.length() - 1);
	}
	cout << "Result: " << maxProd << endl;
	system("PAUSE");
	return 0;
}

string readFile(string fileName) {
	fstream fs(fileName, ios_base::in);
	string retStr = "", tempStr = "";
	while (fs.good()) {
		fs >> tempStr;
		retStr.append(tempStr);
	}
	fs.close();
	return retStr;
}

bool readFive(string* strNum, int* arr) {
	if (strNum->length() < 5) {
		return false;
	}
	else {
		string sStr = strNum->substr(0, 5);
		for (int i = 0, n = sStr.length(); i < n; i++) {
			arr[i] = sStr[i] - '0';
		}
		return true;
	}
}