#include <iostream>
#include <algorithm>
using namespace std;

int main(void) {
	int n = 20, result = 1;
	int* array = new int[n];
	
	for (int i = 0; i < n; i++) {
		array[i] = i + 1;
	}
	
	int nMax = 0;
	while (nMax != 1) {
		nMax = 2 * n;
		for (int i = 0; i < n; i++) {
			if (1 == array[i]) {
				continue;
			}
			else {
				nMax = min(nMax, array[i]);
			}
		}

		if ((2 * n) == nMax)
			break;

		for (int i = 0; i < n; i++) {
			if (array[i] % nMax == 0) {
				array[i] = array[i] / nMax;
			}
		}

		result *= nMax;
	}

	cout << result << endl;
	delete[] array;
	system("PAUSE");
	return 0;
}