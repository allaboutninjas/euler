#include <iostream>
#include <vector>
using namespace std;

int main(void) {
	int n = 2000000, p = 0, i, j;
	long long sum; // very important!
	int* arr = new int[n];
	vector<int> prime;
	
	for (i = 0; i < n; i++) {
		arr[i] = i+1;
	}

	i = 0;
	while (true) {
		j = i;
		while ((1 == arr[j]) && (j < n)) {
			j++;
		}
		if (j >= n) {
			break;
		}
		i = j;
		p = arr[j];
		prime.push_back(p);
		while (j < n) {
			arr[j] = 1;
			j += p;
		}
	}

	for (i = 0, sum = 0, n = prime.size(); i < n; i++) {
		sum += prime[i];
	}

	cout << sum << endl;

	delete[] arr;
	system("PAUSE");
	return 0;
}