#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

int main(int argc, char* argv[])
{
	cout << "Problem 24" << endl;
	cout << "Millionth lexicographic permutation of digits 0123456789 is: ";

	string number = "0123456789";
	int counter = 1;
	int countLimit = 1000000;
	while (counter < countLimit && next_permutation(number.begin(), number.end()))
	{
		++counter;
	}

	cout << number << endl;

	system("PAUSE");
	return 0;
}