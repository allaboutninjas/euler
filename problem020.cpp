#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;

bool sfun(string s1, string s2);
void incrementString(string& s);
string multiplyStrings(string s1, string s2);

int main(void) {
	string result = "1", multiplier = "1";
	cout << multiplyStrings("1231", "13") << endl;
	long int digitSum = 0, i = 1;
	while (i <= 100) {
		result = multiplyStrings(result, multiplier);
		incrementString(multiplier);
		i++;
	}
	for (int i = 0, n = result.length(); i < n; i++) {
		digitSum += result[i] - '0';
	}
	cout << "Sum of digits in (100!): " << digitSum << endl;
	system("PAUSE");
	return 0;
}

bool sfun(string s1, string s2) {
	return s1.length() > s2.length();
}

string multiplyStrings(string s1, string s2) {
	string result = "";
	if (s1.length() < s2.length()) {
		string temp = s1;
		s1 = s2;
		s2 = temp;
	}
	vector<string> partResult;
	int t = 0;
	for (int i = s2.length() - 1; i >= 0; i--) {
		result = "";
		for (int l = partResult.size(); l > 0; l--) {
			result += '0';
		}
		for (int j = s1.length() - 1; j >= 0; j--) {
			t += (s1[j] - '0')*(s2[i] - '0');
			result.insert(0, 1, (t % 10) + '0');
			t /= 10;
		}
		if (t != 0) {
			result.insert(0, 1, t + '0');
			t = 0;
		}
		partResult.push_back(result);
	}

	sort(partResult.begin(), partResult.end(), sfun);
	int len = partResult[0].length();

	for (int i = 0, n = partResult.size(); i < n; i++) {
		for (int j = 0, m = len - partResult[i].length(); j < m; j++) {
			partResult[i].insert(0, 1, '0');
		}
	}

	/*
	for (int i = 0, n = partResult.size(); i < n; i++) {
	cout << partResult[i] << endl;
	}
	*/

	result = "";
	for (int j = partResult[0].length() - 1; j >= 0; j--) {
		for (int i = 0, n = partResult.size(); i < n; i++) {
			t += (partResult[i][j] - '0');
		}
		result.insert(0, 1, (t % 10) + '0');
		t /= 10;
	}
	if (t != 0) {
		result.insert(0, 1, t + '0');
	}
	return result;
}

void incrementString(string& s) {
	// Would be nice to test length and so on
	s[s.length() - 1] += 1;
	for (int i = s.length() - 1; i >= 0; i--) {
		if (s[i] > '9') {
			s[i] = '0';
			if (i > 0) {
				s[i - 1] += 1;
			}
			else {
				s.insert(0, 1, '1');
			}
		}
	}
}

